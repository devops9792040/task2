# Import the "os" library for file manipulation and other functions
import os

# Import the library for manipulating lines within the file
from io import StringIO

# Import functions from modules for different operations
from modules import defNewClient
from modules import defDeleteClient
from modules import defDebitClient
from modules import defDepositClient
from modules import defValueClient
from modules import defExtractClient

# Definition of the main function of the program
def main():
    # Menu options for the user
    print("1 - New Customer\n"
          "2 - Delete Customer\n"
          "3 - Debit\n"
          "4 - Deposit\n"
          "5 - Balance\n"
          "6 - Statement\n\n\n"
          "0 - Exit")

    # Create a variable "menu" that allows the selection of options and request it from the user
    menu = int(input("Enter what you want to do: "))
    
    # Continue to request and call functions based on the user's choice until "0" is entered
    while True:
        if menu == 0:
            break
        elif menu == 1:
            defNewClient.NewCustomer()
            menu = int(input("Enter what you want to do: "))
        elif menu == 2:
            defDeleteClient.DeleteCustomer()
            menu = int(input("Enter what you want to do: "))
        elif menu == 3:
            defDebitClient.Debit()
            menu = int(input("Enter what you want to do: "))
        elif menu == 4:
            defDepositClient.Deposit()
            menu = int(input("Enter what you want to do: "))
        elif menu == 5:
            defValueClient.Balance()
            menu = int(input("Enter what you want to do: "))
        elif menu == 6:
            defExtractClient.Statement()
            menu = int(input("Enter what you want to do: "))
        else:
            print("The entered command is not valid")
            break

# Call the main method to start the application
if __name__ == "__main__":
    main()
