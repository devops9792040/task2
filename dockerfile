# Use an official Python runtime as a base image
FROM python:3.8-slim

# Set the working directory within the container
WORKDIR /app

# Copy your application code and modules into the container
COPY . /app

# Install any required dependencies if you have them
# Example: RUN pip install <dependency>

# Define the command to run your application
CMD ["python", "main.py"]
