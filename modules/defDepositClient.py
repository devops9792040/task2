# Import the "os" library to be able to delete the file and manipulate other functions
import os

# Import the library to manipulate lines within the file
from io import StringIO

# Import the datetime function
from datetime import datetime

# Define the Depositar function (4th option)
def Deposit():
    # Request the necessary variables from the user for making a deposit (CPF and the deposit amount)
    cpf = int(input("Enter the CPF (Taxpayer Identification Number): "))
    value = float(input("Enter the amount to be deposited: "))

    try:
        # Try to open the specified file based on the customer's CPF
        file = open(str(cpf) + ".txt", 'r+')
    except OSError:
        # If it fails to find the file based on the specified CPF, inform the user and return to the menu
        print("File not found")
        print("")
        main()  # You need to define the 'main' function or replace this line with appropriate code.

    # Read the file and store its content in a list
    content = file.readlines()

    # Get the current balance from the file
    balance = float(content[3].rstrip(" \n"))

    # Get the current date and time
    now = datetime.now()
    # Store the date in a formatted string
    date = (str(now.year) + "-" + str(now.month) + "-" + str(now.day) + "-" + str(now.hour) + "-" + str(now.minute) + "-" + str(now.second))

    # Update the balance with the deposit amount
    balance = balance + value

    # Define a function to write to the file
    def write_to_file():
        # After the deposit process, it is necessary to update the balance in the file
        # Replace the balance in line 2 (content[3]) with the new balance
        buffer = StringIO()

        # Open the file with the CPF and make the replacement
        with open(str(cpf) + ".txt", 'r') as stream:
            for index, line in enumerate(stream):
                # 'buffer' is the new variable that will replace line 3
                buffer.write(str(balance) + "\n" if index == 3 else line)

        with open(str(cpf) + ".txt", 'w') as stream:
            # Write the value from 'buffer'
            stream.write(buffer.getvalue())

        # Write the necessary data to the statement: date, deposit, fee, balance
        file.write("\n")
        file.write("Date: %s " % date)
        file.write(" + ")
        file.write(" %.2f " % value)
        file.write(" Fee: 0.00")
        file.write(" Balance %.2f " % balance)

        # Inform the user of the successful deposit
        print("Deposit successfully made")
        print("")

    write_to_file()

    # Close and save the file
    file.close()
