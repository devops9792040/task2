# Import the "os" library to manage files and perform other operations.
import os

# Import the library for manipulating lines within the file.
from io import StringIO

# Function definition for Debit (3rd option).
def Debit():
    # Request necessary variables from the user for debiting (CPF, password, and the debit amount).
    cpf = int(input("Enter the CPF (Taxpayer ID): "))
    password = input("Enter the user's password: ")
    amount = float(input("Enter the amount to be debited: "))

    # Attempt to open the specified file based on the user's CPF.
    try:
        file = open(str(cpf) + ".txt", 'r+')
    # If the file with the specified CPF is not found, display a message to the user and return to the main menu.
    except OSError:
        print("File not found")
        print("")
        main()

    # Read the file and store its content in a list.
    content = file.readlines()

    # Define variables: balance, password, account type, and fee.
    balance = float(content[3].rstrip(" \n"))
    password_from_file = content[4].rstrip(" \n")
    account_type = content[2].rstrip(" \n")
    fee = 0.0

    # Import the date function.
    from datetime import datetime
    now = datetime.now()

    # Store the date in a string variable.
    date = (str(now.year) + "-" + str(now.month) + "-" + str(now.day) + "-" + str(now.hour) + "-" + str(now.minute) + "-" + str(now.second))

    # Define the writing method, if the entered variables are valid.
    def write():
        # After the process, the balance in the file needs to be updated.
        # Replace the balance in line 2 (content[3]) with the new assigned balance.
        buffer = StringIO()

        # Open the file with the CPF and make the replacement.
        with open(str(cpf) + ".txt", 'r') as stream:
            for index, line in enumerate(stream):
                buffer.write(str(balance) + "\n" if index == 3 else line)

        with open(str(cpf) + ".txt", 'w') as stream:
            stream.write(buffer.getvalue())

        # Write the necessary data for the transaction: date, withdrawal or deposit, fee, balance.
        file.write("\n")
        file.write("Date: %s " % date)
        file.write(" - ")
        file.write(" %.2f " % amount)
        file.write(" Fee: %.2f " % fee)
        file.write(" Balance: %.2f " % balance)

        # Inform the user of the successful debit.
        print("Debit successfully completed")
        print("")

    # Check if the password is correct and determine the account type.
    if password_from_file == password:
        # Check the account type to know the maximum allowable debit. If incorrect, return to the menu.
        # If correct, calculate the fee and deduct it from the user's balance.
        if account_type == "Salary":
            fee = 0.05 * amount
            if balance < amount + fee:
                print("It is not possible to debit, as the balance cannot be negative: $ " + str(balance))
                print("")
            else:
                balance = balance - fee - amount
                write()
        elif account_type == "Common":
            fee = 0.03 * amount
            if amount + fee > balance + 500:
                print("It is not possible to debit, as the balance cannot fall below $ 500: " + "$" + str(balance))
                print("")
            else:
                balance = balance - fee - amount
                write()
        elif account_type == "Plus":
            fee = 0.01 * amount
            if amount + fee > balance + 5000:
                print("It is not possible to debit, as the balance cannot fall below $ 5000: " + "$" + str(balance))
                print("")
            else:
                balance = balance - fee - amount
                write()
    else:
        # If the password is incorrect, display a message and return to the menu.
        print("Incorrect password")
        print("")

    # Close the file and save it.
    file.close()
