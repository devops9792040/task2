# Import the "os" library to be able to delete the file and manipulate other functions
import os

# Import the library to manipulate lines within the file
from io import StringIO

# Definition of the ApagaCliente function (2nd option)
def DeleteCustomer():
    # Request the necessary variable (CPF) from the user to delete
    cpf = int(input("Enter the CPF (Taxpayer Identification Number): "))

    # Try to remove the specified file based on the customer's CPF
    try:
        os.remove(str(cpf) + ".txt")
        # Message to inform the user that the file has been removed
        print("File removed")
        print("")
    # If it fails to remove the file based on the specified CPF
    except OSError:
        # Message to inform the user that the file was not found
        print("File not found")
        print("")
