# Import the "os" library to be able to delete the file and manipulate other functions
import os

# Import the library to manipulate lines within the file
from io import StringIO

# Define the Saldo function (5th option)
def Balance():
    # Request the necessary variables from the user
    cpf = int(input("Enter the CPF (Taxpayer Identification Number): "))
    password = input("Enter the user's password: ")

    try:
        # Try to open the specified file based on the customer's CPF
        file = open(str(cpf) + ".txt", 'r+')
    except OSError:
        # If it fails to find the file based on the specified CPF, inform the user and return to the menu
        print("File not found")
        print("")
        #main()  # You need to define the 'main' function or replace this line with appropriate code.

    # Read the file and store its content in a list
    content = file.readlines()

    # Extract the balance, password, and customer's name
    balance = float(content[3].rstrip(" \n"))
    stored_password = content[4].rstrip(" \n")
    customer = content[0].rstrip("\n")

    # Check if the password is correct
    if stored_password == password:
        # Display the customer's name and current balance
        print("")
        print("The current balance for customer", customer, "is: $", balance)
        print("")
    else:
        # Display an error message and return to the menu if the password is incorrect
        print("Incorrect password")
        print("")

    # Close and save the file
    file.close()
