# Import the "os" library to be able to delete the file and manipulate other functions
import os

# Import the library to manipulate lines within the file
from io import StringIO

# Define the NovoCliente function (1st option)
def NewCustomer():
    # Request the necessary variables for creating a new customer (name, CPF, account type, initial balance, password)
    name = input("Enter the name: ")
    cpf = int(input("Enter the CPF (Taxpayer Identification Number): "))
    account_type = input("Enter the account type (Salary, Common, or Plus): ")
    initial_balance = float(input("Enter the initial account balance: "))
    password = input("Enter the user's password: ")

    # Create a file named after the customer's CPF
    file = open(str(cpf) + ".txt", 'w')

    # Check the validity of the account type
    if account_type in ["Salary", "Common", "Plus"]:
        # Write the variables (name, CPF, password, balance, and account type) to the customer's file
        file.write("%s \n" % name)
        file.write("%d \n" % cpf)
        file.write("%s \n" % account_type)
        file.write("%.2f \n" % initial_balance)
        file.write("%s \n" % password)

        # Inform the user that the customer has been successfully created
        print("User created successfully")
        print("")
    else:
        # If the account type is not valid, return to the menu
        print("The account type is not valid")
        print("")

    # Close and save the file after using it
    file.close()
