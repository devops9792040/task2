# Import the "os" library to be able to delete the file and manipulate other functions
import os

# Import the library to manipulate lines within the file
from io import StringIO

# Define the Extrato function (6th option)
def Statement():
    # Request the necessary variables from the user
    cpf = int(input("Enter the CPF (Taxpayer Identification Number): "))
    password = input("Enter the user's password: ")

    try:
        # Try to open the specified file based on the customer's CPF
        file = open(str(cpf) + ".txt", 'r+')
    except OSError:
        # If it fails to find the file based on the specified CPF, inform the user and return to the menu
        print("File not found")
        print("")
        #main()  # You need to define the 'main' function or replace this line with appropriate code.

    # Read the file and store its content in a list
    content = file.readlines()

    # Extract the password and the number of lines in the list
    stored_password = content[4].rstrip(" \n")
    size = len(content)

    # Check if the password is correct
    if stored_password == password:
        # If it's correct, display the customer's name, CPF, account type, and all transactions
        print("")
        print("Name: ", content[0], "CPF: ", content[1], "Account Type: ", content[2])
        # To display the transactions, we skip the password and balance lines
        for line in content[5:size]:
            print(line)
        print("")
    else:
        # If the password is incorrect, display a message and return to the menu
        print("Incorrect password")
        print("")

    # Close and save the file
    file.close()
